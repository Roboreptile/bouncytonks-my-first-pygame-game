# BouncyTonks - my first PyGame game

Pyhton 3.8.0 (UPDATED!)

Game about round ball-like tanks fighting on a generated terrain. The tanks can bouce from the ground!

## Requirements:

- Python 3.8
- Pygame
- Configparser

## Controls:

- LPM - aiming
- RPM - shoot
- 1 - regenerate terrain, restart
- Spacebar - pass round to the second player
- Arrows - move the tank (left, right and UP!)

## Screenshots:

Blue player idle, aiming:

![](demos/demo1.png)

Yellow player attack:

![](demos/demo2.png)
